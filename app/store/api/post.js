const getters = {

}

const state = {
    posts: {},
    response_code: null
}

const mutations = {
    all(state, data) {
        state.posts = data.data;
    },
    create(state, data) {
        state.posts.push(data.data);
    },
    setStatus(state, data) {
        state.response_code = data;
    }
}

const actions = {
    all(context) {
        axios
            .get('/api/posts')
            .then(response => (context.commit('all', response)));
    },
    create(context, data) {
        axios
            .post('/api/posts', data)
            .then(response => (context.commit('create', response)))
            .catch((error) => {
                context.commit('setStatus', error.response.status)
            });
    },
    del(context, data) {
        axios
            .delete('/api/posts/' + data, data)
            .then((response) => (
                state.posts = state.posts.filter(function (item) {
                    return item.id !== data;
                })
            ))
            .catch((error) => {
                context.commit('setStatus', error.response.status)
            });
    }

}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}