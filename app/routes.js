import VueRouter from 'vue-router'

let appLayout = require('./layouts/app.vue');
let authLayout = require('./layouts/auth.vue');

let LandingPage = require('./pages/LandingPage.vue');
let AboutPage = require('./pages/AboutPage.vue');
let PostsPage = require('./pages/PostsPage.vue');

let LoginPage = require('./pages/LoginPage.vue');
let RegistrationPage = require('./pages/RegistrationPage.vue');

let routes = [{
        path: '',
        component: appLayout,
        // name: 'default',
        meta: {
            auth: true
        },
        children: [{
                path: '/',
                component: LandingPage,
                name: 'index',
                meta: {
                    title: 'Главная',
                    icon: 'home'
                }
            },
            {
                path: '/test',
                component: AboutPage,
                name: 'test',
                meta: {
                    title: 'About',
                    icon: 'web_asset'
                }
            },
            {
                path: '/profile',
                component: PostsPage,
                name: 'profile',
                meta: {
                    title: 'Записи',
                    icon: 'library_books'
                }
            }
        ]
    },
    {
        path: '/user',
        component: authLayout,
        meta: {
            auth: true
        },
        children: [{
                path: 'login',
                component: LoginPage,
                name: 'login',
                meta: {
                    title: 'Записи',
                    icon: 'library_books'
                }
            },
            {
                path: 'registration',
                component: RegistrationPage,
                name: 'registration',
                meta: {
                    title: 'Записи',
                    icon: 'library_books'
                }
            }
        ]
    },

];

let router = new VueRouter({
    mode: 'history',
    routes
});

export default router;