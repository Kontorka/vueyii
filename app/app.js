import router from './routes.js'
import store from './store';
import App from './App.vue'
import initPlugins from './plugins';
require('./bootstrap');

window.Vue = require('vue');

const app = {
    render: h => h(App),
    store,
    router
};



// window.app = new Vue({
//     el: '#app',
//     router,
//     store,
//     data: {

//     },
//     render: h => h(App),
//     methods: {
//         isActiveMenu(path) {
//             return window.location.pathname == path;
//         }
//     }
// });

initPlugins(app);

new Vue(app).$mount('#app');