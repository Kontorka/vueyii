<?php

namespace app\controllers;

use dektrium\user\controllers\RegistrationController as BaseRegistrationController;

class RegistrationController extends BaseRegistrationController
{
    public $layout = '/user';
    public function actionCreate()
    {
        // do your magic
    }
}
