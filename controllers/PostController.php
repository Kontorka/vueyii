<?php

namespace app\controllers;

use app\controllers\BaseController;

class PostController extends BaseController
{
    public $modelClass = 'app\models\Posts';
}
